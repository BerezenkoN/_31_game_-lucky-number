import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by user on 08.02.2017.
 */
public class Client {
    private static final String SERVER_HOST = "localhost";
    private static final int SERVER_PORT = 12345;
    private Socket serverSocket;
    private BufferedReader reader;
    private PrintWriter writer;

    public String questServer() {
        String result = null;
        try {
            while ((result = reader.readLine()) == null) ;
            while (reader.ready()) result += "\n" + reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Client() {
        try {
            serverSocket = new Socket(SERVER_HOST, SERVER_PORT);
            reader = new BufferedReader(new InputStreamReader(serverSocket.getInputStream()));
            writer = new PrintWriter(serverSocket.getOutputStream(), true);
            BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
            String message;
            while (!(message = questServer()).contains("Good Bye!")) {
                System.out.println(message);
                writer.println(consoleReader.readLine());
            }
            System.out.println(message);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
                writer.close();
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public static void main(String[] args) {
        Client client = new Client();
    }
}
