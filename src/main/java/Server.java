import GameContainer.GameThread;
import java.io.IOException;
import java.net.ServerSocket;

/**
 * Created by user on 08.02.2017.
 */
public class Server {
    private static int PORT_NUMBER = 12345;
    private static ServerSocket serverSocket;

    private class SocketListener extends Thread {

        public SocketListener() {
            start();
        }

        @Override
        public void run() {
            while (true) {
                try {
                    GameThread gameThread =  new GameThread(serverSocket.accept());
                    System.out.println("New client connected");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public Server() {
        try {
            serverSocket = new ServerSocket(PORT_NUMBER);
            new SocketListener();
            System.out.println("Server started on port: " + PORT_NUMBER);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        Server server = new Server();
    }
}
