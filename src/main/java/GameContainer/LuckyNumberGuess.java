package GameContainer;

import java.util.Random;

/**
 * Created by user on 08.02.2017.
 */
public class LuckyNumberGuess {
    private int gameNumber;
    private int triesCount;

    public int getGameNumber() {
        return gameNumber;
    }

    public int getTriesCount() {
        return triesCount;
    }

    public LuckyNumberGuess(GameSelect gameSelect) {
        Random random = new Random();
        if (gameSelect == GameSelect.Select_1TO100) {
            gameNumber = random.nextInt(100) + 1;
            triesCount = 7;

        }
        if (gameSelect == GameSelect.Select_1TO1000) {
            gameNumber = random.nextInt(1000) + 1;
            triesCount = 9;
        }
    }

    public GameVariation guess(int number) {
        if (--triesCount < 1 && number != gameNumber) return GameVariation.LOSE;
        if (number > gameNumber) return GameVariation.LESS;
        if (number < gameNumber) return GameVariation.MORE;

        return GameVariation.WIN;
    }
}
