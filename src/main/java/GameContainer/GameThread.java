package GameContainer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by user on 08.02.2017.
 */
public class GameThread extends Thread {

    private Socket clientSocket;
    private PrintWriter writer;
    private BufferedReader reader;

    public GameThread(Socket clientSocket) {
        this.clientSocket = clientSocket;
        try {
            writer = new PrintWriter(clientSocket.getOutputStream(), true);
            reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        start();
    }

    @Override
    public void run() {
        writer.println("Hello my friend! Let's play a 'Number Game'");
        writer.println("Select game: press '1' if you choose (1 to 100) / press '2' if you choose (1 to 1000): ");
        int result = 1;
        String str;
        try {
            while ((str = reader.readLine()) == null);
            result = Integer.parseInt(str);
        } catch (IOException e) {
            e.printStackTrace();
        }
        GameSelect gameSelect = GameSelect.values()[result - 1];
        LuckyNumberGuess luckyNumberGuess = new LuckyNumberGuess(gameSelect);
        writer.println("I guessed a 'lucky number' for you, try to Guess! You have " + luckyNumberGuess.getTriesCount() + " tries. Good Luck: ");
        while (true) {
            int number;
            try {
                while ((str = reader.readLine()) == null);
                number = Integer.parseInt(str);
                GameVariation variation = luckyNumberGuess.guess(number);
                if (variation == GameVariation.LESS) writer.println("No, the guessed lucky number is lesser. There are only " + luckyNumberGuess.getTriesCount() + " tries: ");
                if (variation == GameVariation.MORE) writer.println("No, the guessed lucky number is bigger. There are only " + luckyNumberGuess.getTriesCount() + " tries: ");
                if (variation == GameVariation.LOSE) {
                    writer.println("I'm so sorry, but you loose. lucky number is: " + luckyNumberGuess.getGameNumber() + " Good Luck next time!");

                    break;
                }
                if (variation == GameVariation.WIN) {
                    writer.println("Congratulations! You WIN!" +
                            " Now you will have a good day!");
                    break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        try {
            writer.println("Bye bye!!");
            reader.close();
            writer.close();
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
